(function ($, Drupal, PhotoSwipe, PhotoSwipeUI_Default) {

  /**
   * Code taken from http://photoswipe.com/documentation/getting-started.html
   * and adjusted accordingly.
   */
  Drupal.behaviors.photoswipe.openPhotoSwipe = function (index, galleryElement, options) {
    var pswpElement = $('.pswp')[0];
    var items = [];
    options = options || Drupal.behaviors.photoswipe.photoSwipeOptions;

    var images = galleryElement.find('a.photoswipe');
    images.each(function (index) {
      // Create item for every image. We will fill in the src and dimensions later.
      items.push(
        {
          src: '',
          w: '',
          h: '',
          title: $(this).data('overlay-title'),
          pid: $(this).data('pid')
        }
      );
    });

    // Define options.
    options.index = index;
    // Define gallery index (for URL).
    options.galleryUID = galleryElement.data('pswp-uid');

    // Pass data to PhotoSwipe and initialize it.
    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

    // Create variable that will store real size of viewport.
    var firstResize = true,
      imageSrcWillChange,
      devicePixelRatio = '1x',
      activeBreakpoint;

    // BeforeResize event fires each time size of gallery viewport updates.
    gallery.listen('beforeResize', function () {
      // Determine the active multiplier.
      devicePixelRatio = window.devicePixelRatio;
      // Determine the active breakpoint.
      $.each(Drupal.Breakpoints.getCurrent(), function (index, value) {
        if (activeBreakpoint != value.machine_name) {
          activeBreakpoint = value.machine_name;
          imageSrcWillChange = true;
        }
      });

      // Invalidate items only when source is changed and when it's not the first update.
      if (imageSrcWillChange && !firstResize) {
        // InvalidateCurrItems sets a flag on slides that are in DOM, which will force update of content (image) on
        // window.resize.
        gallery.invalidateCurrItems();
      }

      if (firstResize) {
        firstResize = false;
      }

      imageSrcWillChange = false;
    });

    // GettingData event fires each time PhotoSwipe retrieves image source & size.
    gallery.listen('gettingData', function (index, item) {
      // If there is no breakpoint definition, do nothing.
      if (typeof Drupal.settings.responsive_photoswipe[activeBreakpoint] == "undefined") {
        console.log('Missing breakpoint mapping for: ' + activeBreakpoint);
        return;
      }

      // If there is no multiplier definition, check for a fallback possibility.
      if (typeof Drupal.settings.responsive_photoswipe[activeBreakpoint][devicePixelRatio] == "undefined") {
        $.each(Drupal.settings.responsive_photoswipe[activeBreakpoint], function (key, value) {
          devicePixelRatio = key;
        });
      }

      // Set image source & size based on the breakpoint settings.
      item.src = Drupal.settings.responsive_photoswipe[activeBreakpoint][devicePixelRatio][item['pid']].src;
      item.h = Drupal.settings.responsive_photoswipe[activeBreakpoint][devicePixelRatio][item['pid']].h;
      item.w = Drupal.settings.responsive_photoswipe[activeBreakpoint][devicePixelRatio][item['pid']].w;
    });

    gallery.init();
    this.galleries.push(gallery);
  };

})(jQuery, Drupal, PhotoSwipe, PhotoSwipeUI_Default);
